package scripts.Util;

public abstract class Node {
	
	public abstract boolean check();
	
	public abstract void perform();

}
