package scripts;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

import javax.imageio.ImageIO;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Mouse;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Painting;

import scripts.Info.ThieveType;
import scripts.Node.Antiban;
import scripts.Node.Bank;
import scripts.Node.Drop;
import scripts.Node.EatFood;
import scripts.Node.LootChest;
import scripts.Node.PickPocket;
import scripts.Node.RunFromCombat;
import scripts.Node.StealStall;
import scripts.Util.Node;
@ScriptManifest(authors = { "stewiekidz" }, category = "Thieving", name = "skAIO Thiever")
public class Thiever extends Script implements Painting {
	
	private ArrayList<Node> nodes = new ArrayList<Node>();
	public static ThieveType type;
	public static boolean drop;
	private int start_xp;
	private int start_lvl;
	public boolean isRunning = true;
	
	private void loop(int min, int max)
	{
		while(isRunning)
			{
				for(Node n: nodes)
				{
					if(n.check())
					{
						n.perform();
						General.sleep(min, max);
					}
				}
			}
	}
	
	private void init()
	{
		General.useAntiBanCompliance(true);
		Mouse.setSpeed(General.random(150, 180));
		Collections.addAll(nodes, new Antiban(), new Bank(), new Drop(), new EatFood(), new LootChest(), new PickPocket(), new RunFromCombat(), new StealStall());
		start_xp = Skills.getXP(SKILLS.THIEVING);
		start_lvl = Skills.getCurrentLevel(SKILLS.THIEVING);
	}
	
	@Override
	public void run() 
	{
		init();
		Gui gui = new Gui();
		gui.setVisible(true);
		while(gui.isVisible())
		{
			General.sleep(150);
		}
		loop(10,30);
		
	}
	
    //START: Code generated using Enfilade's Easel
    private Image getImage(String url) {
        try {
            return ImageIO.read(new URL(url));
        } catch(IOException e) {
            return null;
        }
    }

    private static final long startTime = System.currentTimeMillis();
    private final Color color1 = new Color(153, 153, 153, 170);
    private final Color color2 = new Color(0, 0, 0);
    private final Color color3 = new Color(153, 102, 255, 170);
    private final Color color4 = new Color(255, 255, 255);
    private final Color color5 = new Color(51, 255, 0);
    private final BasicStroke stroke1 = new BasicStroke(1);
    private final Font font1 = new Font("Leelawadee", 1, 20);
    private final Font font2 = new Font("Leelawadee", 1, 14);
    private final Font font3 = new Font("Leelawadee", 1, 12);
    private final Font font4 = new Font("Raavi", 1, 32);

    private final Image img1 = getImage("http://vignette4.wikia.nocookie.net/2007scape/images/f/f4/Thieving.png/revision/latest?cb=20131026214938");
    @Override
    public void onPaint(Graphics g1) {
    	long timeRan = System.currentTimeMillis() - startTime;
    	int percentTilNextLvl = Skills.getPercentToNextLevel(SKILLS.AGILITY);
        Graphics2D g = (Graphics2D)g1;
        g.setColor(color1);
        g.fillRect(10, 347, 483, 110);
        g.setColor(color2);
        g.setStroke(stroke1);
        g.drawRect(10, 347, 483, 110);
        g.setColor(color3);
        g.fillRect(14, 351, 146, 25);
        g.setColor(color2);
        g.drawRect(14, 351, 146, 25);
        g.setColor(color3);
        g.fillRect(343, 351, 146, 25);
        g.setColor(color2);
        g.drawRect(343, 351, 146, 25);
        g.setColor(color3);
        g.fillRect(181, 351, 146, 25);
        g.setColor(color2);
        g.drawRect(181, 351, 146, 25);
        g.setFont(font1);
        g.setColor(color4);
        g.drawString("Time: " + Timing.msToString(timeRan), 15, 374);
        g.setFont(font2);
        g.drawString("XpGained: " + (Skills.getXP(SKILLS.THIEVING) - start_xp), 183, 371);
        g.setFont(font1);
        g.drawString("Lvls: " + Skills.getCurrentLevel(SKILLS.THIEVING) + " ("+(Skills.getCurrentLevel(SKILLS.THIEVING)- start_lvl) + ") ", 346, 372);
        g.setColor(color5);
        int greenBarWidth = (int) (273.0 * (percentTilNextLvl / 100.0));
        g.fillRoundRect(106, 439, greenBarWidth, 15, 16, 16);
        g.setFont(font3);
        g.setColor(color2);
        g.drawString(" " + percentTilNextLvl +"% TTNL", 224, 451);
        g.setFont(font4);
        g.setColor(color4);
        g.drawString("skAIO Thiever v2.02", 119, 422);
        g.drawImage(img1, 415, 393, null);
        g.drawImage(img1, 32, 396, null);
    }
    //END: Code generated using Enfilade's Easel

}
