package scripts.Node;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.api2007.types.RSItem;

import scripts.Util.Node;

public class EatFood extends Node {

	@Override
	public boolean check() {
		return percentHealth() < 50 && Inventory.getCount(PickPocket.food) >=1;
	}

	@Override
	public void perform() {
		final int LOWHEALTH = Skills.getCurrentLevel(SKILLS.HITPOINTS);
		RSItem[] food = Inventory.find(PickPocket.food);
		
		if(food != null && food.length > 0)
		{
			if(food[0].click("Eat"))
			{
				Timing.waitCondition(new Condition()
				{
					@Override
					public boolean active() {
						return Skills.getCurrentLevel(SKILLS.HITPOINTS) > LOWHEALTH;
					}
				}, General.random(250, 750));
			}
		}
		
	}
	
	private int percentHealth()
	{
		return (Skills.getCurrentLevel(SKILLS.HITPOINTS)/Skills.getActualLevel(SKILLS.HITPOINTS)) * 100;
	}

}
