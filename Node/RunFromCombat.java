package scripts.Node;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;

import scripts.Thiever;
import scripts.Info.Npc;
import scripts.Util.Node;

public class RunFromCombat extends Node {

	@Override
	public boolean check() {
		return Player.getRSPlayer().isInCombat();
	}

	@Override
	public void perform() {

		switch (Thiever.type) {
		case STALL:
			walkAway();
			break;
		case NPC:
			if (PickPocket.npc.equals(Npc.MASTERFARMER)) {
					Inventory.dropAllExcept(keep);
			} else {
				General.sleep((PickPocket.npc.getStunTime() - 1500), (PickPocket.npc.getStunTime() - 500));
			}
			break;
		}

	}

	private void walkAway() {
		if (WebWalking.walkToBank()) {
			Timing.waitCondition(new Condition() {

				@Override
				public boolean active() {
					return !Player.getRSPlayer().isInCombat() || Banking.isInBank();
				}
			}, General.random(1500, 2000));
		}
	}
	
	private String[] keep = {PickPocket.food,"Ranarr seed", "Watermelon seed", "Wildblood seed", "Limpwurt seed", "Toadflax seed","Irit seed", "Avantoe seed", "Kwuarm seed","Snapdragon seed","Cadantine seed","Lantadyme seed","Dwarf weed seed","Torstol seed", "Cactus seed"};

}
