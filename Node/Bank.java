package scripts.Node;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.WebWalking;

import scripts.Thiever;
import scripts.Util.Node;

public class Bank extends Node {

	@Override
	public boolean check() {
		
		switch(Thiever.type)
		{
			case STALL:
				return(!Thiever.drop && Inventory.isFull());
			case NPC:
				return (PickPocket.useFood && Inventory.getCount(PickPocket.food)==0) || (Inventory.isFull());
			case CHEST:
				return Inventory.isFull();
			default:
				return false;
		}
		
	}

	@Override
	public void perform() {
		
		if(!Banking.isInBank())
		{
			WebWalking.walkToBank();
		}
		
		else
		{
			handleBank();
		}
		
	}

	
	public void handleBank()
	{
		if(!Banking.isBankScreenOpen())
			Banking.openBank();
		else if(PickPocket.useFood)
		{
			Banking.depositAll();
			Banking.withdraw(4, PickPocket.food);
			Timing.waitCondition(new Condition()
			{

				@Override
				public boolean active() 
				{
					return Inventory.getCount(PickPocket.food) == 4;
				}
				
			}, General.random(1000, 1500));
		}
		
		else
		{
			Banking.depositAll();
			Timing.waitCondition(new Condition()
			{

				@Override
				public boolean active() 
				{
					return !Inventory.isFull();
				}
			}, General.random(750, 1000));
		}
	}
}
