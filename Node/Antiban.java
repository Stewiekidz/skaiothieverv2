package scripts.Node;
import org.tribot.api.util.ABCUtil;
import org.tribot.api2007.Player;
import org.tribot.api2007.Skills.SKILLS;

import scripts.Gui;
import scripts.Util.Node;

public class Antiban extends Node {

	@Override
	public boolean check() {
		return Player.getAnimation() == -1 || Player.isMoving();
	}

	@Override
	public void perform() {
		abc.performCombatCheck();
		abc.performEquipmentCheck();
		abc.performExamineObject();
		abc.performFriendsCheck();
		abc.performLeaveGame();
		abc.performMusicCheck();
		abc.performPickupMouse();
		abc.performQuestsCheck();
		abc.performRandomMouseMovement();
		abc.performRandomRightClick();
		abc.performRotateCamera();
		abc.performTimedActions(SKILLS.THIEVING);
		abc.performXPCheck(SKILLS.THIEVING);
	}
	
	public static ABCUtil abc = new ABCUtil();

}
