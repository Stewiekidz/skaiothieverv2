package scripts.Node;

import org.tribot.api2007.Inventory;

import scripts.Thiever;
import scripts.Util.Node;

public class Drop extends Node {

	@Override
	public boolean check() {
		return Inventory.isFull() && Thiever.drop;
	}

	@Override
	public void perform() {
		Inventory.dropAllExcept();
		
	}

}
