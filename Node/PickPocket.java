package scripts.Node;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Player;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Walking;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSTile;
import scripts.Thiever;
import scripts.Info.Npc;
import scripts.Info.ThieveType;
import scripts.Util.Node;

public class PickPocket extends Node {

	
	@Override
	public boolean check() {
		
		 return (!Inventory.isFull() && Thiever.type==ThieveType.NPC && !inArea()) ||
				(!Inventory.isFull() && Thiever.type==ThieveType.NPC && inArea());
	}

	@Override
	public void perform() {
		
		if(inArea())
		{
			final int OLDXP = Skills.getXP(SKILLS.THIEVING);
			final RSNPC[] victim = NPCs.findNearest(npc.getName());
			
			 if(victim.length > 0)
			 {
				 if(victim[0].isOnScreen())
				 {
					 if(DynamicClicking.clickRSNPC(victim[0], "Pickpocket"))
					 {
						 Timing.waitCondition(new Condition(){
							@Override
							public boolean active() {
								return Skills.getXP(SKILLS.THIEVING) > OLDXP || !victim[0].isOnScreen();
							}}, General.random(2000, 3500));
						 
						 General.sleep(750,1200);
					 }
				 }
				 else
				 {	
					 if(!victim[0].isOnScreen())
					 	Camera.turnToTile(new RSTile(victim[0].getPosition().getX() + General.random(-4, 4), victim[0].getPosition().getY() + General.random(-4, 4)));
				 	 if(!victim[0].isOnScreen())
				 		Walking.walkTo(victim[0].getPosition());
				 	 Timing.waitCondition(new Condition(){

						@Override
						public boolean active() {
							return victim[0].isOnScreen() || victim[0]==null || Player.getPosition().distanceTo(victim[0]) <= 4;
						}}, General.random(500, 1000));
				 }
			 }
		}
		
		else
		{
			walkToArea();
		}
		
	}
	
	public static Npc npc;
	public static String food;
	public static boolean useFood = false;

	private boolean inArea()
	{
		return npc.getArea().contains(Player.getPosition());
	}
	
	private void walkToArea()
	{
		if(WebWalking.walkTo(npc.getArea().getRandomTile()))
		{
			Timing.waitCondition(new Condition(){

				@Override
				public boolean active() {
					return inArea();
				}}, General.random(3000, 6000));
		}
	}
	
}
