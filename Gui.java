package scripts;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import scripts.Info.Chest;
import scripts.Info.Npc;
import scripts.Info.Stall;
import scripts.Info.ThieveType;
import scripts.Node.LootChest;
import scripts.Node.PickPocket;
import scripts.Node.StealStall;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.Font;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Gui extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui frame = new Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Gui() {
		setTitle("skAIO Thiever v2.01 by Stewiekidz");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 434, 261);
		contentPane.add(tabbedPane);

		// stall pan
		JButton startStall = new JButton("Start");
		JPanel panel = new JPanel();
		tabbedPane.addTab("Stall", null, panel, null);
		panel.setLayout(null);
		startStall.setBounds(175, 171, 89, 23);
		panel.add(startStall);

		final JComboBox comboBoxStall = new JComboBox(Stall.values());
		comboBoxStall.setBounds(126, 79, 182, 23);
		panel.add(comboBoxStall);

		JLabel lblStallChoice = new JLabel("Stall Choice:");
		lblStallChoice.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblStallChoice.setBounds(158, 39, 200, 14);
		panel.add(lblStallChoice);

		final JCheckBox chckbxPowerThieve = new JCheckBox("Power Thieve");
		chckbxPowerThieve.setBounds(175, 125, 97, 23);
		panel.add(chckbxPowerThieve);

		startStall.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				StealStall.stall = (Stall) comboBoxStall.getSelectedItem();
				Thiever.type = ThieveType.STALL;
				Thiever.drop = chckbxPowerThieve.isSelected();
				setVisible(false);

			}
		});

		// npc pan
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Npc", null, panel_1, null);
		panel_1.setLayout(null);

		JButton startNpc = new JButton("Start");
		startNpc.setBounds(175, 171, 89, 23);
		panel_1.add(startNpc);

		final JComboBox comboBoxNpc = new JComboBox(Npc.values());
		comboBoxNpc.setBounds(127, 58, 189, 20);
		panel_1.add(comboBoxNpc);

		textField = new JTextField();
		textField.setBounds(178, 125, 86, 20);
		panel_1.add(textField);
		textField.setColumns(10);

		JLabel lblNewLabel = new JLabel("Npc Choice:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel.setBounds(174, 30, 142, 14);
		panel_1.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Name of Food(Type Exactly, if none leave blank)");
		lblNewLabel_1.setBounds(94, 100, 259, 14);
		panel_1.add(lblNewLabel_1);

		final JCheckBox chckbxUseFood = new JCheckBox("use Food");
		chckbxUseFood.setBounds(39, 124, 97, 23);
		panel_1.add(chckbxUseFood);

		startNpc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Thiever.type = ThieveType.NPC;
				PickPocket.npc = (Npc) comboBoxNpc.getSelectedItem();
				PickPocket.food = textField.getText();
				PickPocket.useFood = chckbxUseFood.isSelected();
				setVisible(false);

			}
		});

		// chest pan
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Chest", null, panel_2, null);
		panel_2.setLayout(null);

		JButton startChest = new JButton("Start");
		startChest.setBounds(175, 171, 89, 23);
		panel_2.add(startChest);

		final JComboBox comboBoxChest = new JComboBox(Chest.values());
		comboBoxChest.setBounds(153, 69, 141, 23);
		panel_2.add(comboBoxChest);

		JLabel lblChestChoice = new JLabel("Chest Choice:");
		lblChestChoice.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblChestChoice.setBounds(165, 39, 129, 14);
		panel_2.add(lblChestChoice);

		startChest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Thiever.type = ThieveType.CHEST;
				LootChest.chest = (Chest) comboBoxChest.getSelectedItem();
				setVisible(false);
			}
		});
	}

}
